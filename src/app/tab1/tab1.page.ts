import { FuncService } from './../service/func.service';
import { QuestionSingleComponent } from './../subpages/question-single/question-single.component';
import { AddQuestionComponent } from './../subpages/add-question/add-question.component';
import { UiService } from './../service/ui.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{
  public allQuestions = [];
  constructor(private uiService: UiService, private funcService: FuncService) {}

  ngOnInit() {
    this.getAllQuestions();
  }
  public openAddQueAns(type) {
    this.uiService.advanceModal(AddQuestionComponent, type);
  }

  public openQuesAns(data) {
    this.uiService.advanceModal(QuestionSingleComponent, data);
  }

  public getAllQuestions( ) {
  this.funcService.getQuesAns().subscribe(data => {
    this.allQuestions = data;
    console.log(data);
  })
  }
}
