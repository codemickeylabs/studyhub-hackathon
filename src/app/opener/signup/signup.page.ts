import { AuthService } from './../../service/auth.service';
import { UiService } from './../../service/ui.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  public registerForm: FormGroup;
  constructor(private uiService: UiService, private authService: AuthService) { }

  ngOnInit() {
    this.registerForm = new FormGroup({
      email: new FormControl('', Validators.compose([
        Validators.required, Validators.email
      ])),
      first_name: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(3)
      ])),
      gender: new FormControl('', Validators.required),
      university: new FormControl('', Validators.required),
      password: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(6)
      ])),
      last_name: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(3)
      ]))
    })
  }

  public goBack() {
    this.uiService.goBackRouter();
  }

  public goToLogin() {
    this.uiService.routingTo('login')
  }

  public signUpUser() {
    this.authService.registerUser(this.registerForm.value);
  }

}
