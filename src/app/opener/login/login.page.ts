import { AuthService } from './../../service/auth.service';
import { UiService } from './../../service/ui.service';

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;
  constructor(private uiService: UiService, private authService: AuthService) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.compose([
        Validators.required,
         Validators.email
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(6)
      ])),
    })
  }

  public goBack() {
    this.uiService.goBackRouter();
  }

  public goToForgot() {
    this.uiService.routingTo('forgotpass')
  }
  public goToSignUp() {
    this.uiService.routingTo('signup')
  }

  public loginUser() {
    this.authService.loginUser(this.loginForm.value);
  }
}
