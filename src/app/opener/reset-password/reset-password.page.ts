import { AuthService } from './../../service/auth.service';
import { UiService } from './../../service/ui.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {
  public forgotForm: FormGroup;
  constructor(private uiService: UiService, private authService: AuthService) { }

  ngOnInit() {
    this.forgotForm = new FormGroup({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.email
      ]))
    })
  }
  public goBack() {
    this.uiService.goBackRouter();
  }

  public goToLogin() {
    this.uiService.routingTo('login');
  }

  public sendReset() {
    this.authService.resetPass(this.forgotForm.value.email);
  }

}
