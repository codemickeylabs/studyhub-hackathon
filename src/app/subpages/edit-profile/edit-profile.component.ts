import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { UiService } from 'src/app/service/ui.service';
import { FuncService } from 'src/app/service/func.service';
// import { FuncService } from './../service/func.service';
// import { UiService } from './../service/ui.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss'],
})
export class EditProfileComponent implements OnInit {

  public data;
  public fname;
  public lname;
  public email;
  public address;
  public country;
  public phone;
  public bio;
  constructor(private uiService: UiService, private funcService: FuncService, private storage: Storage) { }

  ngOnInit() {
    this.fname = this.data['fname'];
    this.lname = this.data['lname'];
    this.email = this.data['email'];
    this.bio = this.data['bio'];
  }

  public goBack() {
    this.uiService.closeModal();
  }

  public async editProfile() {
    const userKey = await this.storage.get('studyhKey');
    const data = {
      fname : this.fname,
      lname : this.lname,
      bio : this.bio,
    }
    if (this.fname !== '' && this.lname !== '') {
      this.funcService.editProfile(userKey, data);
    } else {
      this.uiService.errorToast('Please check your inputs');
    }
  }
}
