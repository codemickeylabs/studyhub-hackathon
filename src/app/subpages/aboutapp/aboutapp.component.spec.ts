import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AboutappComponent } from './aboutapp.component';

describe('AboutappComponent', () => {
  let component: AboutappComponent;
  let fixture: ComponentFixture<AboutappComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutappComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AboutappComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
