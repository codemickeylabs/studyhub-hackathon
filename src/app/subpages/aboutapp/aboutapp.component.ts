import { UiService } from 'src/app/service/ui.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aboutapp',
  templateUrl: './aboutapp.component.html',
  styleUrls: ['./aboutapp.component.scss'],
})
export class AboutappComponent implements OnInit {

  constructor(private uiService: UiService) { }

  ngOnInit() { }

  public goBack() {
    this.uiService.closeModal();
  }

  public openLink() {
    let ahref = document.createElement('a');
    ahref.target = '_blank';
    ahref.href = `https://www.flaticon.com/authors/freepik`;
    ahref.click();
  }
}
