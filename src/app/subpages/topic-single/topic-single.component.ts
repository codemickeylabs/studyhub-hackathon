import { FuncService } from 'src/app/service/func.service';
import { UiService } from './../../service/ui.service';
import { Component, OnInit } from '@angular/core';
import { QuestionSingleComponent } from '../question-single/question-single.component';

@Component({
  selector: 'app-topic-single',
  templateUrl: './topic-single.component.html',
  styleUrls: ['./topic-single.component.scss']
})
export class TopicSingleComponent implements OnInit {
  public data;
  public allQuestions = [];
  constructor(private uiService: UiService, private funcService: FuncService) { }

  ngOnInit() {
    this.funcService.getCategory(this.data['course']).subscribe(data => {
      this.allQuestions = data;
      console.log(data);
    })
  }

  public goBack() {
    this.uiService.closeModal();
  }

  public openQuesAns(data) {
    this.uiService.advanceModal(QuestionSingleComponent, data);
  }
}
