import { Storage } from '@ionic/storage';
import { FuncService } from 'src/app/service/func.service';
import { UiService } from './../../service/ui.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.scss'],
})
export class AddQuestionComponent implements OnInit {
  public data;
  public userKey;
  public topics = [];
  public textContent = '';
  public topicType = '';
  constructor(private uiService: UiService, private funcService: FuncService, private storage: Storage) { }

  ngOnInit() {
    this.topics = [
      {course: 'Computer Science', img: 'computer'},
      {course: 'Electrical Engineering', img: 'electrical'},
      {course: 'Civil Engineering', img: 'civil'},
      {course: 'Business Administration', img: 'business'},
      {course: 'Mechanical Engineering', img: 'mechanical'},
      {course: 'Bio-Technology', img: 'bio'},
      {course: 'Marine Engineering', img: 'marine'},
      {course: 'Animation', img: 'animation'},
      {course: 'Material Science', img: 'material'},
      {course: 'Biology', img: 'biology'},
      {course: 'Accounting', img: 'accounting'},
      {course: 'Chemistry', img: 'chemistry'},
      {course: 'Pharmaceutical Science', img: 'pharmacy'},
      {course: 'Communication Engineering', img: 'communication'},
      {course: 'Law', img: 'law'},
      {course: 'Environmental Engineering', img: 'environment'},
      {course: 'Industrial Design', img: 'design'},
      {course: 'Public Art', img: 'arts'},
      {course: 'Network Engineering', img: 'networking'},
      {course: 'Metallurgical Engineering', img: 'metallurgy'},
    ];
     this.getUser();
  }

  public goBack() {
    this.uiService.closeModal();
  }

  public async getUser() {
    this.userKey = await this.storage.get('studyhKey');
  }

  public submitQueAns() {
    if (this.textContent === '') {
      this.uiService.errorToast(`Please add your ${this.data}`);
    } else if (this.topicType === '') {
      this.uiService.errorToast(`Please choose a topic`);
    } else {
      this.funcService.addQuesAns(this.data, this.textContent,this.userKey, this.topicType);
    }
  }
}
