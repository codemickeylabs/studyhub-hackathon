import { Storage } from '@ionic/storage';
import { FuncService } from 'src/app/service/func.service';
import { UiService } from './../../service/ui.service';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-question-single',
  templateUrl: './question-single.component.html',
  styleUrls: ['./question-single.component.scss'],
})
export class QuestionSingleComponent implements OnInit {
  public data;
  public userData;
  public userKey;
  public answersData = [];
  constructor(private uiService: UiService, private funcService: FuncService,
    private altCtrl: AlertController, private storage: Storage) { }

  ngOnInit() {
    this.getUserProfile();
    this.getUser();
    setTimeout(() => {
      this.getAllAnswers();
    }, 500);
  }

  public goBack() {
    this.uiService.closeModal();
  }

  public getUserProfile() {
    this.funcService.getProfile(this.data.userKey).subscribe(data => {
      this.userData = data;
    })
  }

  public async openAltAns() {
    const openAns = await this.altCtrl.create({
      header: 'Add Answer',
      inputs: [
        {
          type: 'textarea',
          name: 'answer',
          placeholder: 'Type your answer...'
        }
      ],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Submit',
          handler: (ansData) => {
            if (ansData.answer !== '') {
              this.addAnswer(ansData.answer);
            }
            else {
              this.uiService.errorToast('Please add an answer.')
            }
          }
        }
      ]
    });
    return await openAns.present();
  }

  public addAnswer(text) {
    this.funcService.addAnsToQues(this.data.postKey, this.userKey, text);
  }

  public async getUser() {
    this.userKey = await this.storage.get('studyhKey');
  }

  public getAllAnswers() {
    this.funcService.getAnswers(this.data.postKey).subscribe(data => {
      this.answersData = data;
      console.log(data);
    })
  }

  public voteAnswer(ansKey, type) {
    this.funcService.voteAnswer(ansKey, this.userKey, type)
  }
}
