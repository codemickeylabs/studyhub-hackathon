import { AboutappComponent } from './../subpages/aboutapp/aboutapp.component';
import { EditProfileComponent } from './../subpages/edit-profile/edit-profile.component';
import { AuthService } from './../service/auth.service';
import { FuncService } from './../service/func.service';
import { UiService } from './../service/ui.service';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit{
  public userData;
  constructor(private uiService: UiService, private authService: AuthService,
              private funcService: FuncService, private storage: Storage, private modalCtrl: ModalController) { }

  ngOnInit() {
    this.getUserProfile();
  }

  public async openEditProfile() {
    const editModal = await this.modalCtrl.create({
      component: EditProfileComponent,
      componentProps: {data: this.userData}
    });
    return await editModal.present();
  }

  public signOut() {
    this.authService.signOutUser();
  }

  public async getUserProfile() {
    const userKey = await this.storage.get('studyhKey');
    this.funcService.getProfile(userKey).subscribe(data => {
      this.userData = data;
    })
  }

  public async changeImg() {
    const userKey = await this.storage.get('studyhKey');
    this.funcService.changeProfileImg(userKey);
  }

  public goToAbout() {
    this.uiService.defaultModal(AboutappComponent);
  }
}
