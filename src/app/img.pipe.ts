import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'img'
})
export class ImgPipe implements PipeTransform {
  public topics = [
    {course: 'Computer Science', img: 'computer'},
    {course: 'Electrical Engineering', img: 'electrical'},
    {course: 'Civil Engineering', img: 'civil'},
    {course: 'Business Administration', img: 'business'},
    {course: 'Mechanical Engineering', img: 'mechanical'},
    {course: 'Bio-Technology', img: 'bio'},
    {course: 'Marine Engineering', img: 'marine'},
    {course: 'Animation', img: 'animation'},
    {course: 'Material Science', img: 'material'},
    {course: 'Biology', img: 'biology'},
    {course: 'Accounting', img: 'accounting'},
    {course: 'Chemistry', img: 'chemistry'},
    {course: 'Pharmaceutical Science', img: 'pharmacy'},
    {course: 'Communication Engineering', img: 'communication'},
    {course: 'Law', img: 'law'},
    {course: 'Environmental Engineering', img: 'environment'},
    {course: 'Industrial Design', img: 'design'},
    {course: 'Public Art', img: 'arts'},
    {course: 'Network Engineering', img: 'networking'},
    {course: 'Metallurgical Engineering', img: 'metallurgy'},
  ]
  transform(value: any, ...args: any[]): any {
    const index = this.topics.findIndex(e => e.course === value);
    const img = this.topics[index]['img'];
    return img;
  }

}
