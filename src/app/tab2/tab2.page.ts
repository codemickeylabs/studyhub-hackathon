import { TopicSingleComponent } from './../subpages/topic-single/topic-single.component';
import { UiService } from './../service/ui.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{
  public topics = [];
  constructor(private uiService: UiService) {

  }

  ngOnInit() {
    this.topics = [
      {course: 'Computer Science', img: 'computer'},
      {course: 'Electrical Engineering', img: 'electrical'},
      {course: 'Civil Engineering', img: 'civil'},
      {course: 'Business Administration', img: 'business'},
      {course: 'Mechanical Engineering', img: 'mechanical'},
      {course: 'Bio-Technology', img: 'bio'},
      {course: 'Marine Engineering', img: 'marine'},
      {course: 'Animation', img: 'animation'},
      {course: 'Material Science', img: 'material'},
      {course: 'Biology', img: 'biology'},
      {course: 'Accounting', img: 'accounting'},
      {course: 'Chemistry', img: 'chemistry'},
      {course: 'Pharmaceutical Science', img: 'pharmacy'},
      {course: 'Communication Engineering', img: 'communication'},
      {course: 'Law', img: 'law'},
      {course: 'Environmental Engineering', img: 'environment'},
      {course: 'Industrial Design', img: 'design'},
      {course: 'Public Art', img: 'arts'},
      {course: 'Network Engineering', img: 'networking'},
      {course: 'Metallurgical Engineering', img: 'metallurgy'},
    ]
  }

  public openTopic(data) {
    this.uiService.advanceModal(TopicSingleComponent, data);
  }
}
