import { AboutappComponent } from './subpages/aboutapp/aboutapp.component';
import { EditProfileComponent } from './subpages/edit-profile/edit-profile.component';
import { QuestionSingleComponent } from './subpages/question-single/question-single.component';
import { AddQuestionComponent } from './subpages/add-question/add-question.component';
import { FuncService } from './service/func.service';
import { AuthService } from './service/auth.service';
import { UiService } from './service/ui.service';
import { environment } from './../environments/environment';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera/ngx';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { TopicSingleComponent } from './subpages/topic-single/topic-single.component';
import { SharedModule } from './service/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    AddQuestionComponent,
    QuestionSingleComponent,
    TopicSingleComponent,
    EditProfileComponent,
    AboutappComponent
  ],
  entryComponents: [
    AddQuestionComponent,
    QuestionSingleComponent,
    TopicSingleComponent,
    EditProfileComponent,
    AboutappComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    IonicModule.forRoot(), 
    AppRoutingModule,
    FormsModule,
    AngularFireStorageModule,
    ReactiveFormsModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    IonicStorageModule.forRoot()
  ],
  providers: [
    StatusBar,
    SplashScreen,
    UiService,
    AuthService,
    Camera,
    FuncService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
