import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { Injectable, NgZone } from '@angular/core';
import { ModalController, ToastController, LoadingController, NavController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class UiService {

  constructor(private modalCtrl: ModalController, private toastCtrl: ToastController, private loadCtrl: LoadingController,
    private router: Router, private navCtrl: NavController) { }

  public async defaultModal(component) {
    const openModal = await this.modalCtrl.create({
      component: component
    });

    return await openModal.present();
  }

  public async advanceModal(component, data) {
    const openModal = await this.modalCtrl.create({
      component: component,
      componentProps: { data: data }
    });

    return await openModal.present();
  }

  public closeModal() {
    this.modalCtrl.dismiss();
  }

  public async openLoader(msg) {
    const openLoad = await this.loadCtrl.create({
      message: msg,
      showBackdrop: true,
      spinner: 'crescent'
    });

    return await openLoad.present();
  }

  public closeLoader() {
    this.loadCtrl.dismiss();
  }

  public async successToast(msg) {
    const sucToast = await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
      color: 'success'
    });

    return await sucToast.present()
  }

  public async errorToast(msg) {
    const errToast = await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
      color: 'danger'
    });

    return await errToast.present()
  }

  public goBackRouter() {
    this.router.navigateByUrl('/login', {skipLocationChange: true})
  }

  public routingTo(url) {
    this.router.navigateByUrl(`/${url}`, {skipLocationChange: true})
  }

  public rootNav() {
    this.navCtrl.navigateRoot('')
  }

  public logout() {
    this.navCtrl.navigateRoot('/login')
  }
}
