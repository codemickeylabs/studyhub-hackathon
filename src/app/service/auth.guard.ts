import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { UiService } from './ui.service';
import { map, take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router, private uiService: UiService) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if (this.authService.authStateUser !== null) {
        return true
    }

    return this.authService.authState.pipe(take(1)).pipe(map(user => !!user)).pipe(tap(loggedIn => {
        if (!loggedIn) {
            this.router.navigate(['login']);
        }
    }))
  }

}
