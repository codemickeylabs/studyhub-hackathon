import { UiService } from './ui.service';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public createdDate = firebase.firestore.Timestamp.now().seconds;
  constructor(private uiService: UiService, private firestore: AngularFirestore, private auth: AngularFireAuth,
              private storage: Storage) { }

  public loginUser(logindata) {
    this.uiService.openLoader('Logging In...');
    return this.auth.signInWithEmailAndPassword(logindata.email, logindata.password).then((res) => {
      this.storage.set('studyhKey', res.user.uid);
      this.storage.set('loggedIn', true);
      this.uiService.rootNav();
      this.uiService.closeLoader();
    }).catch(err => {
      this.uiService.closeLoader();
      this.uiService.errorToast(err.message);
    });
  }

  public signOutUser() {
    this.auth.signOut().then(() => {
      this.uiService.successToast('Logged Out Successful');
      this.uiService.logout();
      this.storage.remove('studyhKey');
    }).catch(err => {
      this.uiService.errorToast(err.message);
    })
  }

  public registerUser(data) {
    this.uiService.openLoader('Creating Account...');
    return this.auth.createUserWithEmailAndPassword(data.email, data.password).then((res) => {
      this.firestore.collection('studyhubusers').doc(res.user.uid).set({
        email: data.email,
        fname: data.first_name,
        lname: data.last_name,
        university: data.university,
        gender: data.gender,
        photourl: '',
        createdAt: this.createdDate,
        userKey: res.user.uid,
        bio:''
      }).then(() => {
        this.uiService.rootNav();
        this.storage.set('studyhKey', res.user.uid);
        this.storage.set('loggedIn', true);
      });
      this.uiService.closeLoader();
      this.uiService.successToast('Account Created Successful');
    }).catch(err => {
      this.uiService.closeLoader();
      this.uiService.errorToast(err.message);
    });
  }

  public resetPass(email) {
    this.uiService.openLoader('Sending Reset Email...');
    return this.auth.sendPasswordResetEmail(email).then(() => {
      this.uiService.closeLoader();
      this.uiService.goBackRouter();
      this.uiService.successToast('Reset email sent. Please check your email to reset your password.')
    }).catch(err => {
      this.uiService.closeLoader();
      this.uiService.errorToast(err.message);
    })
  }

  get authState(): any {
    return this.auth.authState;
  }

  get authStateUser(): any {
    return this.auth.currentUser
  }
}
