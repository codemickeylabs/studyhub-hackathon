import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { UiService } from './ui.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class FuncService {
  public createdAt = firebase.firestore.Timestamp.now().seconds;
  public camOpts: CameraOptions = {
    destinationType: this.camera.DestinationType.DATA_URL,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    quality: 100,
    encodingType: this.camera.EncodingType.PNG,
    mediaType: this.camera.MediaType.ALLMEDIA,
  }
  constructor(private firestore: AngularFirestore, private uiService: UiService,
    private camera: Camera, private angStorage: AngularFireStorage) { }

  public async changeProfileImg(userKey) {
    this.uiService.openLoader('Uploading Image...')
    const imgData = await this.camera.getPicture(this.camOpts);
    const convertImg = `data:image/jpeg;base64,${imgData}`;
    const imageRef = this.angStorage.ref(`studyhubusers/profile_imgs/${userKey}`);
    (await imageRef.putString(convertImg, 'data_url')).ref.getDownloadURL().then((downL) => {
      this.firestore.collection('studyhubusers').doc(`${userKey}`).update({
        photourl: downL
      }).then(() => {
        this.uiService.closeLoader();
        this.uiService.successToast('Profile Image Updated');
      }).catch(err => {
        this.uiService.errorToast(err.message);
      });
    })
  }

  public editProfile(key, data) {
    this.uiService.openLoader('Saving Profile...');
    return this.firestore.collection('studyhubusers').doc(`${key}`).update({
      fname: data.fname,
      lname: data.lname,
      bio: data.bio,
    }).then(() => {
      this.uiService.closeLoader();
      this.uiService.closeModal();
    }).catch(err => {
      this.uiService.errorToast(err.message);
    })
  }

  public getProfile(key) {
    return this.firestore.collection('studyhubusers').doc(`${key}`).valueChanges();
  }

  public addQuesAns(type, text, uKey, topic) {
    const typeData = {
      createdAt: this.createdAt,
      text: text,
      type: type,
      userKey: uKey,
      topic: topic,
    }
    this.uiService.openLoader('Adding question...');
    return this.firestore.collection('quesans').add(typeData).then((res) => {
      this.uiService.closeLoader();
      this.uiService.closeModal();
      this.uiService.successToast('Added Successfully');
      this.firestore.collection('quesans').doc(`${res.id}`).update({
        postKey: res.id
      });
    }).catch(err => {
      this.uiService.closeLoader();
      this.uiService.errorToast(err.message);
    });
  }

  public getQuesAns() {
    return this.firestore.collection('quesans', ref => ref.orderBy('createdAt', 'desc')).valueChanges();
  }

  public getCategory(type) {
    return this.firestore.collection('quesans', ref => ref.orderBy('createdAt', 'desc').where('topic', '==', `${type}`)).valueChanges();
  }

  public addAnsToQues(postKey, userKey, text) {
    this.uiService.openLoader('Adding answer...')
    const data = {
      pKey: postKey,
      ownKey: userKey,
      answer: text,
      upvotes: 0,
      downvotes: 0,
      createdAt: this.createdAt
    }
    return this.firestore.collection('answers').add(data).then((res) => {
      this.firestore.collection('answers').doc(`${res.id}`).update({
        ansKey: res.id
      });
      this.uiService.closeLoader();
      this.uiService.successToast('Answer Added Successfully.');
    }).catch(err => {
      this.uiService.closeLoader();
      this.uiService.errorToast(err.message);
    })
  }

  public getAnswers(postKey) {
    return this.firestore.collection('answers', ref => ref.where('pKey', '==', `${postKey}`).orderBy('upvotes', 'desc')).valueChanges();
  }


  public voteAnswer(ansKey, userKey, type) {
    const voteData = {
      uKey: userKey,
      answerkey: ansKey,
      createdAt: this.createdAt,
      voteType: type
    }

    return this.firestore.collection('votes', ref => ref.where('answerkey', '==', `${ansKey}`).where('uKey', '==', `${userKey}`)).get().toPromise().then((res) => {
      if (res.empty === true) {
        this.firestore.collection('answers', ref => ref.where('ansKey', '==', `${ansKey}`)).get().toPromise().then((ansData) => {
          ansData.docs.map(ans => {
            let mainAns = ans.data();
            let upVoteAns = mainAns['upvotes']
            let downVoteAns = mainAns['downvotes']
            if (type === 'upvote') {
              let newUpVote = upVoteAns + 1
              this.firestore.collection('answers').doc(`${ansKey}`).update({
                upvotes: newUpVote
              }).then(() => {
                this.firestore.collection('votes').add(voteData).then((voteDone) => {
                  this.firestore.collection('votes').doc(voteDone.id).update({
                    voteKey: voteDone.id
                  });
                });
              });
            } else if (type === 'downvote') {
              let newDownVote = downVoteAns + 1;
              this.firestore.collection('answers').doc(`${ansKey}`).update({
                downvotes: newDownVote
              }).then(() => {
                this.firestore.collection('votes').add(voteData).then((voteDone) => {
                  this.firestore.collection('votes').doc(voteDone.id).update({
                    voteKey: voteDone.id
                  });
                });
              });
            }
          })
        });
      } else {
        this.firestore.collection('votes', ref => ref.where('answerkey', '==', `${ansKey}`).where('uKey', '==', `${userKey}`)).get().toPromise().then((aldyvote) => {
          aldyvote.docs.map(alres => {
            if (type === alres.data()['voteType']) {
              let voteChanged = `${type}d`;
              this.uiService.errorToast(`You have already ${voteChanged}`);
            } else if (type === 'upvote' && alres.data()['voteType'] === 'downvote') {
              this.firestore.collection('answers', ref => ref.where('ansKey', '==', `${ansKey}`)).get().toPromise().then((aData) => {
                aData.docs.map(maData => {
                  let upVAns = maData.data()['upvotes'] + 1;
                  let downVAns = maData.data()['downvotes'] - 1;
                  this.firestore.collection('answers').doc(`${ansKey}`).update({
                    upvotes: upVAns,
                    downvotes: downVAns
                  }).then(() => {
                    this.firestore.collection('votes').doc(`${alres.data()['voteKey']}`).update({
                      voteType: 'upvote'
                    })
                  })
                })
              })
            } else if (type === 'downvote' && alres.data()['voteType'] === 'upvote') {
              this.firestore.collection('answers', ref => ref.where('ansKey', '==', `${ansKey}`)).get().toPromise().then((aData) => {
                aData.docs.map(maData => {
                  let upVAns = maData.data()['upvotes'] - 1;
                  let downVAns = maData.data()['downvotes'] + 1;
                  this.firestore.collection('answers').doc(`${ansKey}`).update({
                    upvotes: upVAns,
                    downvotes: downVAns
                  }).then(() => {
                    this.firestore.collection('votes').doc(`${alres.data()['voteKey']}`).update({
                      voteType: 'downvote'
                    })
                  })
                })
              })
            }
          });
        });
      }
    });
  }
}
