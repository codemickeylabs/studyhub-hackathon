import { ImgPipe } from './../img.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        ImgPipe
    ],
    exports: [
        ImgPipe
    ]
})
export class SharedModule {}