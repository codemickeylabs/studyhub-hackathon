// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDWWERq7nxuQiOQ5tfeaj3Ft3wC4T7Bqe0",
    authDomain: "studyhub-app.firebaseapp.com",
    databaseURL: "https://studyhub-app.firebaseio.com",
    projectId: "studyhub-app",
    storageBucket: "studyhub-app.appspot.com",
    messagingSenderId: "22261261665",
    appId: "1:22261261665:web:025b4f7e0538c2553ed120",
    measurementId: "G-DG80QJ3F75"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
